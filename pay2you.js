/**
 * Данный скрипт предназначен для автоматического заполнения формы перевода средств
 * на сервисе pay2you.com.uaю Скрипт работает только на странице 
 * https://p2y.deltabank.com.ua/p2uweb.html#lang=ru&bank=#/.
 *
 * Чтобы Заполнить форму нужными ланными нужно:
 * 1. изменить значения переменных ниже
 * 2. открыть в браузере адрес https://p2y.deltabank.com.ua/p2uweb.html#lang=ru&bank=#/
 * 3. открыть консоль разработчика или firebug
 * 4. во вкладке "Console" вставить этот скрипт целиком с нужными данными и нажать Enter
 */
(function($) {
		// Номер карты отправителя, пробелы обязательны
	var cc_number = '4111 1111 1111 1111',
		// Дата окончания действия карты отправителя, слэш обязателен
		exp_date = '07/17',
		// CVV-код карты отправителя
		cvv = '321',
		// Сумма перевода
		amount = 492.54,
		// Номер карты получателя, пробелы обязательны
		cc_number_2 = '4111 1111 1111 1111',
		// Номер телефона
		phone = '+380631234567';

	cc_number = cc_number.split(' ');
	exp_date = exp_date.split('/');
	cc_number_2 = cc_number_2.split(' ');
	
	$(function() {
		// cc number
		$('#cardA1').val(cc_number[0]);
		$('#cardA2').val(cc_number[1]);
		$('#cardA3').val(cc_number[2]);
		$('#cardA4').val(cc_number[3]);

		// exp_date
		$('#monCardA').val(exp_date[0]);
		$('#yerCardA').val(exp_date[1]);

		// cvv
		$('#cvvCardA').val(cvv);

		// amount
		$('#amountTrans').val(amount);

		// cc number 2
		$('#cardB1').val(cc_number_2[0]);
		$('#cardB2').val(cc_number_2[1]);
		$('#cardB3').val(cc_number_2[2]);
		$('#cardB4').val(cc_number_2[3]);

		// phone
		$('#phoneA').val(phone);

		// terms & conditions
		$('#accepted').attr('checked', 'checked');

		// submit
		$('#send').click();
	});
})(jQuery);
